<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APIController extends Controller
{
    public function examp()
    {
        return response()->json([
            'meta' => [
                'code' => 200,
                'status' => 'success',
                'message' => 'Example API request successful!',
            ],
            'data' => [
                'name' => 'John Doe',
                'email' => 'john.doe@example.com',
            ],
        ]);
    }
}
