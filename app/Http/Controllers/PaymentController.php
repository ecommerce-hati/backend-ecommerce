<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Xendit\Invoice\InvoiceApi;
use Xendit\Configuration;
// use Xendit\Xendit;


class PaymentController extends Controller
{
    var $apiInstance = null;
    public function __construct()
    {
        Configuration::setXenditKey('xnd_development_VY3yUyPwZE4djQly66rOyXNm9qn7j15uP0aaKluuuY9jmI8GEPAXQCAIrvLjmnZ');
        $this->apiInstance = new InvoiceApi();
        // Xendit::setApiKey("xnd_development_VY3yUyPwZE4djQly66rOyXNm9qn7j15uP0aaKluuuY9jmI8GEPAXQCAIrvLjmnZ");
    }

    public function store(Request $request)
    {

        // $params = [
        //     'external_id' => (string) Str::uuid(),
        //     'payer_email' => $request->payer_email,
        //     'description' => $request->description,
        //     'amount' => $request->amount,
        //     'redirect_url' => 'bursa-kain.com'
        // ];

        // dd($request);
        // $items = [
        //     [
        //         'name' => "sepatu",
        //         'quantity' => 1,
        //         'price' => 20000
        //     ],
        //     [
        //         'name' => "sandal",
        //         'quantity' => 2,
        //         'price' => 30000

        //     ],
        // ];

        // $apiInstance = new InvoiceApi();
        $createInvoice = new \Xendit\Invoice\CreateInvoiceRequest([
            'external_id' => (string) Str::uuid(),
            'description' => $request->description,
            'amount' => $request->amount,
            'items' => $request->items,
            // 'invoice_duration' => 172800,
            'currency' => 'IDR',
            // 'reminder_time' => 1
        ]);


        $result = $this->apiInstance->createInvoice($createInvoice);

        // Save to database
        $payment = new Payment;
        $payment->status = 'pending';
        $payment->checkout_link = $result['invoice_url'];
        $payment->external_id = $createInvoice['external_id'];
        $payment->save();

        return response()->json($payment);

        // \Xendit\Invoice\CreateInvoiceRequest
        // $for_user_id = "8438e679-646f-4612-b9cb-c3d26b787fd9"; // string | Business ID of the sub-account merchant (XP feature)

        // $createInvoice = new \Xendit\Invoice\CreateInvoiceRequest($params);

        // try {
        //     $result = $apiInstance->createInvoice($createInvoice, $for_user_id);
        //     print_r($result);
        // } catch (\Xendit\XenditSdkException $e) {
        //     echo 'Exception when calling InvoiceApi->createInvoice: ', $e->getMessage(), PHP_EOL;
        //     echo 'Full Error: ', json_encode($e->getFullError()), PHP_EOL;
        // }

        // Save to database
        // $payment = new Payment;
        // $payment->status = 'pending';
        // $payment->checkout_link = $createInvoice['invoice_url'];
        // $payment->external_id = $params['external_id'];
        // $payment->save();

        // return response()->json(['data' => $createInvoice['invoice_url']]);
    }

    public function callback(Request $request)
    {
        $result = $this->apiInstance->getInvoices(null, $request->external_id);

        // return response()->json(['data' => $result]);


        // get data
        $payment = Payment::where('external_id', $request->external_id)->firstOrFail();

        if ($payment->status == 'settled') {
            return response()->json(['status' => 'Pembayaran Anda Telah Di Proses', 'data' => $result]);
        }

        // update status
        $payment->status = strtolower($result[0]['status']);
        $payment->save();

        // return redirect()->to('https://ecommerce-hati-baru.vercel.app/Profile/Riwayat-Pembelian');

        // return redirect('http://localhost:3000/Profile/Riwayat-Pembelian');

        return response()->json(['status' => 'Sukses', 'data' => $result]);
    }

    public function testApi()
    {
        return response()->json(['status' => 'berhasil']);
    }
}
