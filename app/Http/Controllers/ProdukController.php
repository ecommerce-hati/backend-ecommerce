<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;
use App\Http\Resources\ProdukShowRequest;
use App\Models\MmbarangJenis;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ProdukController extends Controller
{

    /**
     * @OA\Get(
     *     path="/produk",
     *     operationId="getProdukList",
     *     tags={"Produk"},
     *     summary="Get a list of produk",
     *     description="Returns list of projects",
     *     @OA\Response(
     *         response=200,
     *         description="Everything is fine",
     *         @OA\Items(ref="#/components/schemas/ProdukShowRequest"),
     *     ),
     * )
     */
    public function index(Request $request)
    {

        DB::beginTransaction();
        try {
            $data = DB::select("call sp_product_list('$request->jenis_kategori', '$request->jenis_kain', '$request->jenis_proses', '$request->grade', '$request->color')");
            // $data = DB::select("call sp_product_top_list(null,null, null, null, null,2, 4);");

            DB::commit();
            return response()->json([
                'success'   => true,
                'data'      => $data,
                'message'   => "Berhasil mengambil data"
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success'   => true,
                'data'      => $e->getMessage(),
                'message'   => "Gagal mengambil data"
            ], 400);
        }
    }

    public function indexWithFilter(Request $request)
    {

        DB::beginTransaction();
        try {
            $data = DB::select("call sp_product_top_list(null,null, null, null, null,'$request->tipe', '$request->limit')");
            // $data = DB::select("call sp_product_top_list(null,null, null, null, null,2, 4);");

            DB::commit();
            return response()->json([
                'success'   => true,
                'data'      => $data,
                'message'   => "Berhasil mengambil data"
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success'   => true,
                'data'      => $e->getMessage(),
                'message'   => "Gagal mengambil data"
            ], 400);
        }
    }

    public function detail(Request $request)
    {

        DB::beginTransaction();
        try {
            $data = DB::select("call sp_product_detail_m('$request->id')");
            // $data = DB::select("call sp_product_top_list(null,null, null, null, null,2, 4);");

            DB::commit();
            return response()->json([
                'success'   => true,
                'data'      => $data,
                'message'   => "Berhasil mengambil data"
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success'   => true,
                'data'      => $e->getMessage(),
                'message'   => "Gagal mengambil data"
            ], 400);
        }
    }

    public function jenis_kain(Request $request)
    {
        try {
            //code...
            $data = MmbarangJenis::where('is_fashion', '=', $request->is_fashion)->get();
            return response()->json([
                'success'   => true,
                'data'      => $data,
                'message'   => "Berhasil mengambil data"
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success'   => true,
                'data'      => $e->getMessage(),
                'message'   => "Gagal mengambil data"
            ], 400);
        }
        // DB::beginTransaction();
        // try {
        //     $data = DB::select("call sp_product_detail_m('$request->id')");
        //     // $data = DB::select("call sp_product_top_list(null,null, null, null, null,2, 4);");

        //     DB::commit();
        //     return response()->json([
        //         'success'   => true,
        //         'data'      => $data,
        //         'message'   => "Berhasil mengambil data"
        //     ], 200);
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     return response()->json([
        //         'success'   => true,
        //         'data'      => $e->getMessage(),
        //         'message'   => "Gagal mengambil data"
        //     ], 400);
        // }
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_produk' => ['required'],
            'harga'       => ['required', 'numeric'],
            'kategory'    => ['required'],
            'brand'       => ['required'],
            'size'        => ['required'],
            'color'       => ['required'],
            'rating'      => ['required'],
            'options'     => ['required'],
            'image'       => ['required'],
        ]);

        if ($validate->fails()) {
            return response()->json($validate->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $produk = Produk::create($request->all());
            $res = [
                'message' => 'Produk Baru Telah Ditambahkan',
                'data'    => $produk,
            ];

            return response()->json($res, Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed ' . $e->errorInfo
            ]);
        }
    }

    /**
     * @OA\Get(
     *     path="/produk/{id}",
     *     tags={"Produk"},
     *     summary="Get produk by ID",
     *     description="Returns project data",
     *     @OA\Response(
     *         response=200,
     *         description="Everything is fine",
     *         @OA\JsonContent(ref="App\Models\Produk")
     *     ),
     * )
     *
     */
    public function show($id)
    {
        $produk = Produk::where('id', $id)->get();
        $res = [
            'message' => 'Ambil Data Produk Dengan Id',
            'data'    => $produk,
        ];

        return response()->json($res, Response::HTTP_OK);
    }

    /**
     * @OA\Put(
     *     path="/produk/{id}",
     *     tags={"Produk"},
     *     summary="Update produk by ID",
     *     description="Returns project data",
     *     @OA\Response(
     *         response=200,
     *         description="Everything is fine",
     *         @OA\JsonContent(ref="App\Models\Produk")
     *     ),
     * )
     *
     */
    public function update(Request $request, $id)
    {
        $produk = Produk::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'nama_produk' => ['required'],
            'harga'       => ['required', 'numeric'],
            'kategory'    => ['required'],
            'brand'       => ['required'],
            'size'        => ['required'],
            'color'       => ['required'],
            'rating'      => ['required'],
            'options'     => ['required'],
            'image'       => ['required'],
        ]);

        if ($validate->fails()) {
            return response()->json($validate->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $produk->update($request->all());
            $res = [
                'message' => 'Barang Keluar telah terupdate',
                'data'    => $produk,
            ];

            return response()->json($res, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed ' . $e->errorInfo
            ]);
        }
    }

    /**
     * @OA\Delete(
     *     path="/produk/{id}",
     *     operationId="getProdukById",
     *     tags={"Produk"},
     *     summary="Delete produk by ID",
     *     description="Returns project data",
     *     @OA\Response(
     *         response=200,
     *         description="Everything is fine",
     *         @OA\JsonContent(ref="App\Models\Produk")
     *     ),
     * )
     *
     */
    public function destroy($id)
    {
        $produk = Produk::where('id', $id);

        try {
            $produk->delete();
            $res = [
                'message' => 'Produk Telah Terhapus'
            ];

            return response()->json($res, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed ' . $e->errorInfo
            ]);
        }
    }
}
