<?php

// namespace App\Http\Controllers;
namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    // public function __construct(User $user)
    // {
    //     $this->user = $user;
    // }

    public function index()
    {
        return response()->json([
            'meta' => [
                'code' => 200,
                'status' => 'success',
                'message' => 'User fetched successfully!',
            ],
            'data' => [
                'user' => User::all(),
            ],
        ]);
    }

    public function tes()
    {
        return response()->json([
            'meta' => [
                'code' => 200,
                'status' => 'success',
                'message' => 'User fetched successfully!',
            ],
        ]);
    }
}
