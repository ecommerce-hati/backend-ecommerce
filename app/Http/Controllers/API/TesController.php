<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TesController extends Controller
{
    public function example()
    {
        return response()->json([
            'meta' => [
                'code' => 200,
                'status' => 'success',
                'message' => 'Example API request successful!',
            ],
            'data' => [
                'name' => 'John Doe',
                'email' => 'john.doe@example.com',
            ],
        ]);
    }
}
