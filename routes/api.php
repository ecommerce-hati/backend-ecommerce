<?php

use App\Http\Controllers\APIController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\TesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::get('auth', [App\Http\Controllers\AuthController::class, 'redirectToAuth']);
Route::get('auth/callback', [App\Http\Controllers\AuthController::class, 'handleAuthCallback']);
Route::get('/user', [App\Http\Controllers\UserController::class, 'index']);
Route::get('/user-tes', [App\Http\Controllers\UserController::class, 'tes']);
Route::get('/example', [TesController::class, 'example']);
Route::get('/examp', [APIController::class, 'examp']);
Route::get('/produk', [ProdukController::class, 'index']);
Route::get('/produk/filter', [ProdukController::class, 'indexWithFilter']);
Route::get('/produk/detail', [ProdukController::class, 'detail']);
Route::get('/produk/jenis-kain', [ProdukController::class, 'jenis_kain']);

Route::post('/payments', [PaymentController::class, 'store']);
Route::post('/payments/callback', [PaymentController::class, 'callback']);
Route::get('/payments/testing', [PaymentController::class, 'testApi']);




// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/register', [App\Http\Controllers\AuthController::class, 'register']);
// Route::post('/login', [App\Http\Controllers\AuthController::class, 'login']);

// Route::middleware('auth:api')->group(function() {
//     Route::get('/me', [App\Http\Controllers\UserController::class, 'me']);
//     Route::post('/logout', [App\Http\Controllers\AuthController::class, 'logout']);
// });

// Route::name('.')->group(function () {
//     Route::prefix('produk')->name('produk.')->group(function () {
//         Route::resource('/', App\Http\Controllers\ProdukController::class)->parameter('', 'id');
//     });
// });
